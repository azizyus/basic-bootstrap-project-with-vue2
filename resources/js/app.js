

//Bootstrap

require('./bootstrap');
require('bootstrap/dist/js/bootstrap.bundle');




import VueRouter from 'vue-router'

//Vue Js
window.Vue = require('vue');

//Bootstrap Vue
Vue.use(require('bootstrap-vue'));
Vue.use(VueRouter);


import VueLang from '@eli5/vue-lang-js';

const default_locale = window.lang;
const fallback_locale = window.lang;
const messages = '';



Vue.use(VueLang, {
    messages: messages, // Provide locale file
    locale: default_locale, // Set locale
    fallback: fallback_locale // Set fallback locale
})

//Fragment
import Fragment from 'vue-fragment'
Vue.use(Fragment.Plugin)


import PortalVue from 'portal-vue'
Vue.use(PortalVue);

const moment = require('moment/min/moment-with-locales');
Vue.use(require('vue-moment'),{
    moment
});

const router = new VueRouter({
    routes:[
        {
            path: '/',
            meta:{},
            name:'index',
            component: require('../components/index').default,
        },
        {
            path: '/example-page',
            meta:{},
            name:'exampleComponent',
            component: require('../components/example-component.vue').default,
        },
    ],
    mode: 'history',
});

window.app  = new Vue({
    router,
    mounted() {

    },
    el: "#app",
    mixins:[
    ],
    methods: {
        exampleCallback()
        {
            this.inputValue = 'CALLED';
        },
        goToExampleComponent()
        {
          this.$router.push({name:'exampleComponent'});
        },
    },
    computed:{
    },
    components: {
    },

    data()
    {
        return {
            inputValue:null,
        };
    },

});

